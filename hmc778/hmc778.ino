#include <SPI.h>




#define REFCLK 100
#define R_DIVIDER 100 
#define PRESCALLER 2
class HMC778 {

public:  
  int freqInMhz;
  int phDetectorFreq; 
  int Rdiv;
  int IntG;
  double Nfrac;
  int frac;
  
  int _ssPin =4;
  int clkPin = 18;
  int dataInputPin = 19;
  int dataPin = 23;
  int cenPin = 2;
  int valueR = 0;
  HMC778(){}
  HMC778(int freqInMhz ){
    calculateDivider(freqInMhz);
    printDividers();
    chipEnable();
    initialize();

    loadDividers();
    //testGPO();
    return;
    while(1){
      myblink2();
      //WriteRegister32(0x80000000);
      delay(100);
    }
    }
  void chipEnable(){
    digitalWrite(cenPin, LOW);
    pinMode(cenPin, OUTPUT);
    digitalWrite(cenPin, LOW);
    digitalWrite(cenPin, HIGH);
    }
  
  void csLow(){
    digitalWrite(_ssPin, LOW);
    }
  void csHigh(){
    digitalWrite(_ssPin, HIGH);
    }
  void csInit(){
    pinMode(_ssPin, OUTPUT);
    csLow();
    }
  void initialize()
  {
    csInit();
    chipEnable();
    
    SPI.begin();              // Init SPI bus
    SPI.setDataMode(SPI_MODE0);      // CPHA = 0  Clock positive
    SPI.setBitOrder(MSBFIRST);
    SPI.setClockDivider (SPI_CLOCK_DIV64);
    delay(10);
   
  }

  void WriteRegister32(const uint32_t value){
    valueR = 0;
    csHigh();
    for (int i = 3; i >= 0; i--){
      valueR |= SPI.transfer((value >>( 8 * i)) & 0xFF)<<(i*8);
//      Serial.print(">>");
//      Serial.println(SPI.transfer((value >>( 8 * i)) & 0xFF),HEX);
    }
    csLow();
    valueR = valueR >> 1;
    delay(1);
    Serial.println(valueR,HEX);
  }

  void calculateDivider(int freqInMhz){
    int freq = freqInMhz /PRESCALLER ;
    phDetectorFreq = REFCLK / R_DIVIDER;
    Rdiv = R_DIVIDER;
    IntG = freq / phDetectorFreq;
    Nfrac = freq % phDetectorFreq;
    frac = Nfrac * pow(2,24);
    }
  void WriteRegister(int addr, int val){
      WriteRegister32((addr<<25) + (val << 1));
    }
  void loadDividers(){
    printDividers();
      WriteRegister(0x02,100);
      WriteRegister(0x03,2310);
//       WriteRegister(0x03,46);
      WriteRegister(0x04,frac);
     // 111 1 1 0 1111 1
        // 111 1101 1111
   //   7df

        WriteRegister(0x06,0x73f);// set frac
        WriteRegister(0x07,0x104862);// set pd settings 
     WriteRegister(0x08,0x3ffff);// rf divider /2 en
 WriteRegister(0x0f ,9);

//       WriteRegister(0x0f ,21); // lock detect
        while(1){
            WriteRegister(0x0f ,0x9); // lock detect
           delay(500);
            WriteRegister(0x0f ,10); // lock detect
           delay(500);
           
            WriteRegister(0x0f ,1); // lock detect
           delay(500);
          
        }
    }  
  void testGPO(){
    for ( int i = 0 ; i < 30 ; i++){
      
       WriteRegister(0x0f ,i); // lock detect
       Serial.println(i);
       delay(2000);
      }

  }
  void printDividers(){
    Serial.printf("freqInMhz: %d, phDetectorFreq: %d\n", freqInMhz, phDetectorFreq);
    Serial.printf("Rdiv: %d, intG: %d, frac: %d \n", Rdiv, IntG, frac);
    }  
  void myblink(){
   
   WriteRegister32(0x1e000040);
   delay(500);
   WriteRegister32(0x1e000000);
   delay(500);
  }
   void myblink2(){
   
   WriteRegister(0x0f, 0x20);
   delay(500);
   WriteRegister(0x0f ,0x00);
   delay(500);
  }
  struct R2{
      int rdiv:14;
  };
};
void setup() {
 Serial.begin(115200);
 Serial.println("Hi!");
 HMC778 hmc778(9700);
 

}


void loop() {

 Serial.println("Hi!");
}
